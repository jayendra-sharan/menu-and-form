var MenuView = (function() {
  function MenuView(model) {
    this.model = model;
  }

  MenuView.prototype.createMenuView = function (links, className) {
    var ulElem = document.createElement('ul');
    ulElem.className = className;

    var liStr = "";
    links.forEach(function(item) {
      if (item === "+") {
        liStr += '<li id="dropdown">' + item + '</li>';
      } else if (item.toLowerCase() === "home") {
        liStr += '<li class="active">' + item + '</li>';
      } else {
        liStr += '<li>' + item + '</li>';
      }
    });

    ulElem.innerHTML = liStr;

    return ulElem;
  };

  return MenuView;
})();
