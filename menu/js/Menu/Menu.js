;(function (global, MenuModel, MenuView, MenuController){

  var MenuComponent = function () {
    
  }

  MenuComponent.prototype.init = function (menuArray) {
    this.menuLinks = menuArray;
    var menuModel = new MenuModel(menuArray);
    var menuView = new MenuView(menuModel);
    var menuCtrl = new MenuController (menuView, menuModel);

    menuCtrl.init();
    // console.log('Menu Component Initialized', this.menuLinks);
  };

  MenuComponent.prototype.createMenu = function () {
    // this.menuCtrl.initMenu();
  };

  var menuComponent = new MenuComponent ();
  global.menuComponent = menuComponent;
})(window, MenuModel, MenuView, MenuController);
