var MenuController = (function() {
  function MenuController (view, model) {
    this.view = view;
    this.model = model;
    this.$navigationElem = "";
    this.actualMenuLength = 0;
    this.currentItemCount = 0;
    this.subMenuShow = false;
  }

  function _cacheDom () {
    this.$navigationElem = $('#main-navigation');
  };

  function _bindEvents () {
    this.$navigationElem.on('click', _onMenuClick.bind(this));
    window.onresize = _resizeHandler.bind(this);
  }

  function _onMenuClick (event) {
    var isCollapsedMenu = (event.target.parentElement.className === "collapsed-menu") ? true : false;
    if (event.target.id === "dropdown" && !isCollapsedMenu) {
      this.subMenuShow = !this.subMenuShow;
      event.target.children[0].style.display = this.subMenuShow ? 'block' : 'none';
    } else if (event.target.id === "dropdown" && isCollapsedMenu) {
      this.collapsedMenuShow = !this.collapsedMenuShow;

      var liMenus = this.$navigationElem.find('li');
      liMenus[0].innerHTML = (liMenus[0].textContent === "+") ? "-" : "+";
      for (var i = 1; i < liMenus.length; i++) {
        liMenus[i].style.display = this.collapsedMenuShow ? 'block' : 'none';
      }
    } else {
      this.$navigationElem.find('li').removeClass('active');
      event.target.classList.add('active');
    }


  }

  var _resizeHandler = _debounce(_updateMenuView, 25);

  function _updateMenuView(event) {
    var menuLinks = [],
        verticalMenuLinks = [];
    var viewWidth = document.documentElement.offsetWidth;
    var li = this.$navigationElem.find('li');

    var liWidth = 150;

    // (viewWidth - liWidth): 1 li width for displaying + plus button
    var numOfAllowedMenuItems = Math.floor((viewWidth - liWidth) / liWidth);

    if (viewWidth < 450){
      // show collapsible menu
      menuLinks = this.model.getMenuLinks();
      _showCollapsedMenu.call(this, menuLinks);
    } else {
        /**
        *  conditions:
        *  1. if the actualMenuLength is less than numOfAllowedMenuItems on the screen
        *    - display original menu, the screen is wide enough.
        *  2. Also, when the currentItemCount is not equal to numOfAllowedMenuItems on the screen,
        *    - re-render the menu.
        *  3. Else, if check 2, and get the new menu items and render.
        */

        if (this.actualMenuLength <= numOfAllowedMenuItems) {
          menuLinks = this.model.getMenuLinks();
          _renderMenu.call(this, menuLinks);
        } else {

          menuLinks = this.model.getVisibleMenuItems(numOfAllowedMenuItems);
          _renderMenu.call(this, menuLinks);

          verticalMenuLinks = this.model.getHiddenMenuItems(numOfAllowedMenuItems)
          _appendVerticalMenu.call(this, verticalMenuLinks)
        }
    }
  }

  MenuController.prototype.init = function () {
    _cacheDom.call(this);
    _bindEvents.call(this);
    this.actualMenuLength = this.model.getMenuLength();
    this.currentItemCount = this.model.getMenuLength();

    _renderMenu.call(this, this.model.getMenuLinks());    // initialize
    _updateMenuView.call(this);                           // then update
  };

  /**
   *  @description function to render menu ul, li items.
   *  @param  {Array} menuLinks is an array of menu items.
   *  @param  {Boolean} createHorizontal determines if the collapsed/hidden menu
   *                    should be created or not.
   *  @return {void}
   */

  function _renderMenu (menuLinks) {
    this.currentItemCount = menuLinks.length;
    if (this.actualMenuLength !== this.currentItemCount) {
        menuLinks.splice(menuLinks.length-1, 0, "+");
    }

    var menu = this.view.createMenuView(menuLinks, "main-navigation-links");
    this.$navigationElem.find('ul').remove();
    this.$navigationElem.append(menu);
    this.$navigationElem.append(menu);
  }

  function _appendVerticalMenu (menuLinks) {
      var menu = this.view.createMenuView(menuLinks, "sub-navigation-links");
      this.$navigationElem.find('ul').find('#dropdown').find('ul').remove();
      this.$navigationElem.find('ul').find('#dropdown').append(menu);
  }

  function _showCollapsedMenu (menuLinks) {
    var modifiedMenu = menuLinks.slice(0);
    modifiedMenu.unshift('+');
    var menu = this.view.createMenuView(modifiedMenu, 'collapsed-menu');
    this.$navigationElem.find('ul').remove();

    // hide all except the first li

    var liMenus = menu.children;

    for (var i = 1; i < liMenus.length; i++) {
      liMenus[i].style.display = this.collapsedMenuShow ? 'block' : 'none';
    }

    this.$navigationElem.append(menu);
  }

  function _activateMenuItem(target) {

    var liElems = this.$navigationElem.find('li');

    for (var i = 0; i < liElems.length; i++) {
      liElems[i].removeClass('active');
    }

    event.target.className = 'active';
  }



  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  function _debounce(func, wait, immediate) {
  	var timeout;
  	return function() {
  		var context = this, args = arguments;
  		var later = function() {
  			timeout = null;
  			if (!immediate) func.apply(context, args);
  		};
  		var callNow = immediate && !timeout;
  		clearTimeout(timeout);
  		timeout = setTimeout(later, wait);
  		if (callNow) func.apply(context, args);
  	};
  };

  return MenuController;
})();
