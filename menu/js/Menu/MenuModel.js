var MenuModel = (function(){
  function MenuModel (data) {
    this.data = data;
  };

  MenuModel.prototype.getMenuLinks = function () {
    return this.data;
  };

  MenuModel.prototype.getMenuLength = function () {
    return this.data.length;
  };

  MenuModel.prototype.getVisibleMenuItems = function (numOfItems) {
    var links = [];
    links = [].concat(this.data.slice(0, numOfItems - 1)).concat(this.data.slice(-1));

    return links;
  };

  MenuModel.prototype.getHiddenMenuItems = function (numOfItems) {
    var links = [];
    links = this.data.slice(numOfItems - 1, this.data.length - 1);
    return links;
  };

  return MenuModel;
})();
