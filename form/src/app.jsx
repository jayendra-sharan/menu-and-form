import React from 'react';
import '../styles/index.scss';

import JsonForm from './jsonForm/JsonForm';

const jsonData = {
  "data":
    {
      "text": "This is a sample note",
      "date": "2017-05-10",
      "occurrence": "Every year"
    }
}


export default class App extends React.Component {
  render() {
    return (
      <div>
        <JsonForm data={jsonData.data}/>
      </div>
    )
  }
}
