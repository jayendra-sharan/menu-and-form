import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Occurence extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: [],
      selected: ""
    }
  }

  componentWillMount () {
    const options = [
      "Everyday",
      "Every week",
      "Every year",
      "None"
    ];
    this.setState({
      options,
      selected: this.props.defaultValue
    })
  }

  /**
   *
   */

  _onOptionSelect (event) {
    const selected = event.target.value;
    this.setState({
      selected
    });
    this.props.onChange(event, selected);
  }

  render () {
    return (
      <select
        value={this.state.selected}
        onChange={this._onOptionSelect.bind(this)}>
        {
          this.state.options.map(item => {
            return (
              <option key={item} value={item}>{item}</option>
            )
          })
        }
      </select>
    )
  }
}

Occurence.propTypes = {
  defaultValue: PropTypes.string,
  onChange: PropTypes.func.isRequired
}

export default Occurence;
