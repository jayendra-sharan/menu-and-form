import React, { Component } from 'react';
import PropTypes from 'prop-types';

class AlertBox extends Component {
  constructor(props) {
    super(props);
  }
  /**
   *  @description function handle the submit action of the form
   *  @param  {Object} event object
   *  @return {void}
   */

  _onClick () {
      this.props.onClick();
  }

  render () {
    return (
      <div className="alert-box-modal">
        <div className="alert-box-content">
          <div className="alert-box-title">
            {this.props.title}
          </div>

          <div className="alert-box-message">
            {this.props.message}
          </div>

          <div className="alert-box-action">
            <button
              type="button"
              className="btn btn-prim"
              onClick={this._onClick.bind(this)}>
                Ok
            </button>
          </div>
        </div>
      </div>
    )
  }
}

AlertBox.propTypes = {
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

export default AlertBox;
