import React, { Component } from 'react';
import PropTypes from 'prop-types';

class NoteField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      count: 0
    }
  }

  componentWillMount () {
    const {text} = this.props;
    const count = text.length
    this.setState({
      text,
      count
    })
  }

  /**
   *  @description event handler for onChange event on textarea
   *  @param  {Object}  event object
   */

  _onChange (event) {
    const text = event.target.value;
    const count = text.length;
    if (count <= 100) {
      this.setState({
        text,
        count
      });
    }
    this.props.onChange(event, text);
  }

  render () {
    return (
      <div>
        <textarea
          className={this.props.className}
          name="textarea"
          rows="5"
          cols="40"
          onChange={this._onChange.bind(this)}
          value={this.state.text}>

        </textarea>
        <span className="form-text-count">{this.state.count}</span>
      </div>
    )
  }
}

NoteField.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
  onChange: PropTypes.func.isRequired

}

export default NoteField;
