import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { DateField } from 'react-date-picker'
import 'react-date-picker/index.css'

// custom imports
import NoteField from './NoteField';
import Occurence from './Occurence';
import AlertBox from './AlertBox';


class JsonForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      date: "",
      occurrence: "",
      showAlert: false,
      data: ""
    }
  }

  componentWillMount () {
    const {text, date, occurrence} = this.props.data;
    this.setState({
      text,
      date,
      occurrence
    });
  }

  /**
   *  @description function handle the submit action of the form
   *  @param  {Object} event object
   *  @return {void}
   */

  _onSubmit (event) {
    event.preventDefault();
    const data = {
      text: this.state.text,
      date: this.state.date,
      occurrence: this.state.occurrence
    }
    this.setState({
      showAlert: true,
      data: JSON.stringify(data)
    });
  }

  /**
   *  @description function to handle onChange event on note field
   *  @param  {Object} event object
   *  @param  {string} text received from note field
   *  @return {void}
   */

  _onNoteChange (event, text) {
    this.setState({
      text
    });
  }

  /**
   *  @description function to handle onChange event on date field
   *  @param  {Object} date enetered in the date picker
   *  @return {void}
   */

   _onDateChange (date) {
     this.setState({
       date
     });
   }

   /**
    *  @description function to handle onChange event on Occurence field
    *  @param  {Object} event object
    *  @param  {string} text received from drop down field
    *  @return {void}
    */

   _onOccurrenceChange (event, occurrence) {
     this.setState({
       occurrence
     });
     if (occurrence.toLowerCase() === "none") {
       const date = new Date();
       this.setState({
         date: date.toLocaleDateString()
       })
     }
   }

 /**
  *  @description function handle the submit action of alert using props
  *  @param  {Object} event object
  *  @return {void}
  */

  _onClick () {
      this.setState({
        showAlert: false
      });
  }

  render () {
    return (
      <div className="form-component">
        <form method="POST" onSubmit={this._onSubmit.bind(this)}>

          <NoteField
            className="form-text"
            text={this.state.text}
            onChange={this._onNoteChange.bind(this)}/>

          <span className="form-date-picker">
            <span className="form-text">Select Date: </span>

            <DateField
              dateFormat="YYYY-MM-DD"
              value={this.state.date}
              onChange={this._onDateChange.bind(this)}
            />

          </span>

          <span className="form-occurrence">
            <span className="form-text">Select Occurence: </span>

            <Occurence
              defaultValue={this.state.occurrence}
              onChange={this._onOccurrenceChange.bind(this)}
            />

          </span>
          <div className="form-buttons">
            <button type="submit" id="submit">Submit</button>
          </div>
        </form>

        {
          this.state.showAlert &&
          <AlertBox
            title="Alert"
            message={this.state.data}
            onClick={this._onClick.bind(this)} />
        }
      </div>
    )
  }
}

JsonForm.propTypes = {
  data: PropTypes.object
}

export default JsonForm;
